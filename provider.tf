
provider "aws" {
  region = "ap-southeast-2" # Sydney
}

terraform {
  backend "s3" {
    bucket         = "tcc-stream-v10-state"
    key            = "management/terraform.tfstate"
    region         = "ap-southeast-2"
    dynamodb_table = "tcc-stream-v10-state-locking"
  }
}

