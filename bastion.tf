
module "bastion" {
  source             = "git::https://gitlab.com/tcc-version-1.0/tfm-bastion.git?ref=v1.7"
  ami_id             = "ami-0cdfb861146115b31"
  subnet_id          = aws_subnet.az-a-public.id
  security_group_ids = [aws_security_group.ssh.id]
  keyname            = aws_key_pair.michaelc.key_name
}

