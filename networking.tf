
resource "aws_route_table" "igw" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = {
    Name = local.tag_name
  }
}

resource "aws_route_table" "natgw" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gw.id
  }
}

resource "aws_subnet" "az-a-public" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "ap-southeast-2a"
  map_public_ip_on_launch = true

  tags = {
    Name = "${local.tag_name}-public"
  }
}

resource "aws_route_table_association" "az-a-public" {
  subnet_id      = aws_subnet.az-a-public.id
  route_table_id = aws_route_table.igw.id
}

resource "aws_subnet" "az-b-public" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "ap-southeast-2b"
  map_public_ip_on_launch = true

  tags = {
    Name = "${local.tag_name}-public"
  }
}

resource "aws_route_table_association" "az-b-public" {
  subnet_id      = aws_subnet.az-b-public.id
  route_table_id = aws_route_table.igw.id
}

resource "aws_subnet" "az-a-private" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.3.0/24"
  availability_zone       = "ap-southeast-2a"
  map_public_ip_on_launch = false

  tags = {
    Name = "${local.tag_name}-private"
  }
}

resource "aws_route_table_association" "az-a-private" {
  subnet_id      = aws_subnet.az-a-private.id
  route_table_id = aws_route_table.natgw.id
}

resource "aws_subnet" "az-b-private" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.4.0/24"
  availability_zone       = "ap-southeast-2b"
  map_public_ip_on_launch = false

  tags = {
    Name = "${local.tag_name}-private"
  }
}

resource "aws_route_table_association" "az-b-private" {
  subnet_id      = aws_subnet.az-b-private.id
  route_table_id = aws_route_table.natgw.id
}
