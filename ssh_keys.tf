
data "local_file" "michaelc-ssh-pubkey" {
  filename = "/home/michaelc/.ssh/id_rsa.pub"
}

resource "aws_key_pair" "michaelc" {
  key_name   = "michaelc"
  public_key = data.local_file.michaelc-ssh-pubkey.content
}

