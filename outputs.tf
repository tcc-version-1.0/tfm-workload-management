
output "vpc" {
  value = aws_vpc.main.id
}

output "subnet-az-a-public" {
  value = aws_subnet.az-a-public.id
}

output "subnet-az-b-public" {
  value = aws_subnet.az-b-public.id
}

output "subnet-az-a-private" {
  value = aws_subnet.az-a-private.id
}

output "subnet-az-b-private" {
  value = aws_subnet.az-b-private.id
}

output "key_name" {
  value = aws_key_pair.michaelc.key_name
}

output "sg-all-outbound" {
  value = aws_security_group.outbound-to-all.id
}
output "sg-http" {
  value = aws_security_group.http.id
}
output "sg-https" {
  value = aws_security_group.https.id
}
output "sg-ssh" {
  value = aws_security_group.ssh.id
}
output "sg-ssh-internal" {
  value = aws_security_group.ssh-internal.id
}

output "bastion-host-eip" {
  value = module.bastion.eip
}
